/*

aethred.js - Convert text reqwuests to Aethred responses.
Copyright (C) 2022  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
// import fetch from 'node-fetch';
import { Configuration, OpenAIApi } from 'openai';

const MAX_CHARS = 400;
const MAX_LOOPS = 10;

const configuration = new Configuration({
  apiKey: process.env.OPENAI_KEY,
});
const openai = new OpenAIApi(configuration);

const EIGHT_BALL_RESPONSES = [
  'It is certain.', 'It is decidedly so.', 'Without a doubt.', 'Yes definitely.', 'You may rely on it.',
  'As I see it, yes.', 'Most likely.', 'Yes.', 'Signs point to yes.',
  'Reply hazy, try again.', 'Ask again later.', 'Better not tell you now.', 'Cannot predict now.', 'Concentrate and ask again.',
  'Don\'t count on it.', 'My reply is no.', 'Outlook not so good.', 'Very doubtful'
];

async function responseGeneration(prompt) {
	const response = await openai.createCompletion({
	  model: process.env.OPENAI_MODEL,
	  prompt: `Human: Hello, how are you today?\nShakespearean Chatbot: How now!\n###\nHuman: What is your favorite season?\nShakespearean Chatbot: Love, whose month is ever May.\n###\nHuman: What is love? \nShakespearean Chatbot: Love looks not with the eyes, but with the mind, and therefore is wing'd cupid painted blind.\n###\nHuman: I need some advice on love. \nShakespearean Chatbot: The course of true love never did run smooth.\n###\nHuman: I have unrequited love problems. \nShakespearean Chatbot: The charter of thy worth gives thee releasing;\nMy bonds in thee are all determinate.\nFor how do I hold thee but by thy granting?\nAnd for that riches where is my deserving?\nThe cause of this fair gift in me is wanting,\nAnd so my patent back again is swerving.\n###Human: ${prompt}`,
	  temperature: 0,
	  max_tokens: 1579,
	  top_p: 1,
	  frequency_penalty: 0,
	  presence_penalty: 0,
	  stop: ['###'],
  });

  return response.data.choices[0].text.replaceAll('Shakespearean Chatbot:', '');
}

export const aethred = async (text) => {
  if (text.includes('magic 8 ball')) {
    return EIGHT_BALL_RESPONSES[Math.floor(Math.random()*EIGHT_BALL_RESPONSES.length)];
  } else {
    const groups = /(?:(?<!\<\/a\>\<\/span\>).)+(.+)\<\/p\>/.exec(text);
    const prompt = `${groups[1]}###`;
    let response = responseGeneration(prompt);

    let loops = 0;
    while (response.length > MAX_CHARS && loops < MAX_LOOPS) {
      response = responseGeneration(prompt);
      loops++;
    }

    if (loops == MAX_LOOPS) {
      response = 'Flippity flop!';
    }

    return response;
  }
}
