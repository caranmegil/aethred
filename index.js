/*

index.js - The driver behind the Aethred chat bot.
Copyright (C) 2022  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import 'dotenv/config';

import express from 'express';
import bodyParser from 'body-parser';

import { login } from 'masto';
import { aethred} from './aethred.js';

const HOST = process.env.MASTO_HOST;
const TOKEN = process.env.MASTO_TOKEN;
const NAME = process.env.MASTO_NAME;
// const LINGUA_HOST = process.env.LINGUA_HOST;
const PORT = process.env.PORT || 3000;

let app = express();
app.use(bodyParser.json());

app.post('/', async function(req, res) {
  const message = await aethred(req.body.text);
  res.json({response: message});
});

app.listen(PORT, () => {
  console.log(`Aethred host listening on port ${PORT}`);
});

function sleep(timer) {
	return new Promise(resolve => setTimeout(resolve, timer));
}


async function main() {
	const masto = await login( {
		url: `https://${HOST}`,
		accessToken: TOKEN,
		timeout: 1000,
	});

	const stream = await masto.stream.streamUser();

	return stream.on('notification', async notification => {
		const message = await aethred(notification.status.content);
		
		if (notification.type === 'mention') {
			const replyUser = `@${notification.account.acct}`;
			const visibility = notification.status.visibility;
			const reply = message;
			const rate = (reply.length / 18)*1000;
			// sleep(rate);
			masto.statuses.create({
				status: `${replyUser} ${reply}`,
				inReplyToId: notification.status.id,
				visibility,
			});
		}
	});
};

main().catch((e) => {
	console.log(e);
	process.exit(1);
});

