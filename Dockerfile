FROM node:16

WORKDIR /usr/aethred/
COPY . /usr/aethred
RUN npm ci
CMD [ "node", "index.js" ]
